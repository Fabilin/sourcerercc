if [ -z $1 ];
then
	echo "Usage: $0 <srczip>"
	exit
fi

cd tokenizers/file-level
echo "../../$1" > project-list.txt
rm -r bookkeeping_projs files_stats files_tokens logs
python2 tokenizer.py zip
find files_tokens -name "*" -type f -exec cat '{}' > ../../clone-detector/input/dataset/blocks.file +
cd ../../clone-detector
/usr/bin/time -v -o "../$1.time" python2 controller.py
cd ..
find -regex "clone-detector/NODE_.*?/output8\\.0/query_.*" -exec cat '{}' > results.pairs +
